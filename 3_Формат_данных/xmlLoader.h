#ifndef XMLLOADER_H
#define XMLLOADER_H

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <jansson.h>
#include "list.h"

typedef struct {
	char name[50];
	int age;
	float rating; 
	float time;
} Telecast;


void ListTelecast_Print( List * list);
void Telecast_print(Telecast * self);

Telecast * Telecast_newFromString(char * str);
Telecast * Telecast_new(char * name, int age, float rating, float time);
void printToFile(const char * fileName, char * buffer);
void XmlLoader_saveToFile(const char * fileName, List * list);
List * Storage_loadXML(const char * fileName, List * list);

#endif