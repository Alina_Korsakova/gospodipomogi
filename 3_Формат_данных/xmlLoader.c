#include "xmlLoader.h"
#include "list.h"

Telecast * Telecast_new(char * name, int age, float rating, float time){
	Telecast * self = malloc(sizeof(Telecast));
	strcpy(self->name, name);
	self->age = age;
	self->rating = rating;
	self->time = time;
	return self;
}

bool readAllText(const char * filename, char * text) {
	char line[100];
    
    FILE * fr = fopen(filename, "r");
	if (fr == NULL) return false;
    while(fgets(line, 100, fr)) {
        strcat(text, line);
    }
	fclose(fr);
	return true;
}

void Telecast_free(Telecast ** selfPtr){
	free(*selfPtr);
	*selfPtr = NULL;
}

void printToFile(const char * fileName, char * buffer) {
	FILE* fout = fopen(fileName, "w");
	fprintf(fout, "%s\n", buffer);
	fclose(fout);
}

Telecast * Telecast_newFromString(char * str) {
	Telecast * self = Telecast_new("", 0, 0.0, 0.0);
	sscanf(str, "%s %i %f %f", 
		self->name, 
		&(self)->age,
		&(self)->rating,
		&(self)->time );
	return self;
}

void XmlLoader_saveToFile(const char * fileName, List * list){
	xmlDoc * doc = NULL;
	xmlNode * rootNode = NULL;
	xmlNode * listNode = NULL;
	xmlNode * infoNode = NULL;
	char buffer[1000] = "\0";
	char strBuf[100];
	
	doc = xmlNewDoc(BAD_CAST "1.0");

	// create xml tree

	// create one root element
	rootNode = xmlNewNode(NULL, BAD_CAST "telecasts");
	xmlDocSetRootElement(doc, rootNode);
	
	for (int i = 0; i < List_count(list); i++) {
		// planet child
		Telecast * op = (Telecast *)List_get(list, i);

		listNode = xmlNewChild(rootNode, NULL, BAD_CAST "Telecast", NULL);
		xmlNewChild(listNode, NULL, BAD_CAST "name", BAD_CAST op->name);
		sprintf(strBuf, "%i", op->age);  // copy number to string
		xmlNewChild(listNode, NULL, BAD_CAST "age", BAD_CAST strBuf);
		sprintf(strBuf, "%f", op->rating);  // copy number to string
		xmlNewChild(listNode, NULL, BAD_CAST "rating", BAD_CAST strBuf);
		sprintf(strBuf, "%f", op->time);  // copy number to string
		xmlNewChild(listNode, NULL, BAD_CAST "time", BAD_CAST strBuf);
		// create info element as operator child
		
		// copy xml contents to char buffer
		xmlBuffer * bufferPtr = xmlBufferCreate();
		xmlNodeDump(bufferPtr, NULL, (xmlNode *)doc, 0, 1);
		strcpy(buffer, (char *)bufferPtr->content);
		xmlBufferFree(bufferPtr);
	}
	
    xmlFreeDoc(doc);
	printToFile(fileName, buffer);

}

void Telecast_print(Telecast * self){
	printf("\nName: %s, Age: %i, Rating: %g, Time: %g",
		self->name, 
		self->age,
		self->rating,
		self->time
		);
		//Planet_free();
}

void ListTelecast_Print( List * list)
{
	if (!List_isEmpty(list)) {
		for (int i = 0; i < List_count(list); i++) {
			printf("\nOperator #%i", i+1);
			Telecast_print(List_get(list, i));
		}
	} else 
		printf("List is empty");
}

List * Storage_loadXML(const char * fileName, List * list) {
	int numberOfOperator = 0;
	char text[1000] = "";

	// for (int i = 0; i < List_count(list); i++) {
	// 	Telecast * telecast = (Telecast *)List_get(list, i);
	// 	Telecast_free(&telecast);
	// }
	// List_clear(list);
	
	if (!readAllText(fileName, text)) {
		printf("Error reading %s\n", fileName);
		//return;
	}
	xmlDoc * xDoc = xmlReadMemory(text, strlen(text), NULL, NULL, 0);
	if (NULL == xDoc) {
		printf("Error parsing xml");
		return NULL;
	}


	xmlNode * xRootEl = xmlDocGetRootElement(xDoc);
	for(xmlNode * xCur = xRootEl->children; NULL != xCur ; xCur = xCur->next) {
		if (XML_ELEMENT_NODE == xCur->type) {
			Telecast * op = Telecast_new("", 0, 0.0, 0.0);
			for (xmlNode * xJ = xCur->children; NULL != xJ ; xJ = xJ->next) {
				if (XML_ELEMENT_NODE == xJ->type) {
					char * content = (char *)xmlNodeGetContent(xJ);
					
					if (xmlStrcmp(xJ->name, BAD_CAST "name") == 0) {
						xmlNode * xplanet = xJ;
						xmlNode * xName = xplanet->children->next;
						char * name = (char *)xmlNodeGetContent(xName);
						//operators[numberOfOperator].name = (char *)name;
						strcpy(op->name, (char *)content);
					}
					if (xmlStrcmp(xJ->name, BAD_CAST "age") == 0) {
						char * temp = xmlNodeGetContent(xJ);
						int age = atoi(temp);
						op->age = age;
						free(temp);
					}
					if (xmlStrcmp(xJ->name, BAD_CAST "rating") == 0) {
						char * temp = xmlNodeGetContent(xJ);
						float rating = atof(temp);
						op->rating = rating;
						free(temp);
					}
					if (xmlStrcmp(xJ->name, BAD_CAST "time") == 0) {
						char * temp = xmlNodeGetContent(xJ);
						float time = atof(temp);
						op->time = time;
						free(temp);
					}
					xmlFree(content);
				}
			}
			List_add(list, op);
			numberOfOperator++;
		}
	}
	xmlFreeDoc(xDoc);
	return list;
}