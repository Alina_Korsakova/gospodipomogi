#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "xmlLoader.h"
#include "list.h"
#include <libxml/parser.h>
#include <libxml/tree.h>

#define BUFFER_SIZE 1024

int main(void) {
	
	List * telecasts = List_new();
	List * telecasts1 = List_new();
	List * telecasts2 = List_new();

	 Telecast telecast = {
	 	.name = "Pink Guy",
	 	.age = 47,
	 	.rating = 14.88,
	 	.time = 20.01
	 };

	 Telecast telecast1 = {
	 	.name = "Keklya",
	 	.age = 32,
	 	.rating = 34.87,
	 	.time = 12.15
	 };
	 Telecast telecast2 = {
	 	.name = "Zhdi menya",
	 	.age = 13,
	 	.rating = 99.99,
	 	.time = 18.54
	 };


	List_add(telecasts, &telecast);
	List_add(telecasts, &telecast1);
	List_add(telecasts, &telecast2);

	XmlLoader_saveToFile("out.xml", telecasts);
    
	Storage_loadXML("out.xml", telecasts1);

	for(int i = 0; i < List_count(telecasts); i++){
		Telecast *t = List_get(telecasts,i);
		Telecast *t1 = List_get(telecasts1,i);
		if( strcmp(t->name, t1->name) != 0 && 
			t->age != t1->age &&
			t->rating != t1->rating &&
			t->time != t1->time)
			{
				puts("Not equal");
				break;
			}
			
	}

	return 0;
}
