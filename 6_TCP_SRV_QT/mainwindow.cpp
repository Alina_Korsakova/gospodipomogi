#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
    server_status = 0;
}

void MainWindow::on_starting_clicked()
{
    tcpServer = new QTcpServer(this);

    connect(tcpServer, SIGNAL(newConnection()), this, SLOT(newuser()));

    if (!tcpServer->listen(QHostAddress::Any, 33333) && server_status==0) {
        qDebug() <<  QObject::tr("Unable to start the server: %1.").arg(tcpServer->errorString());
        ui->textinfo->append(tcpServer->errorString());
    } else {
        server_status = 1;
        qDebug() << tcpServer->isListening() << "TCPSocket listen on port";
        ui->textinfo->append(QString::fromUtf8("Сервер запущен!"));
        qDebug() << QString::fromUtf8("Сервер запущен!");
    }
}

void MainWindow::on_stoping_clicked()
{
    if(server_status == 1){
        foreach(int i,SClients.keys()){
            QTextStream os(SClients[i]);
            os.setAutoDetectUnicode(true);
            os << QDateTime::currentDateTime().toString() << "\n";
            SClients[i]->close();
            SClients.remove(i);
        }
        tcpServer->close();
        ui->textinfo->append(QString::fromUtf8("Сервер остановлен!"));
        qDebug() << QString::fromUtf8("Сервер остановлен!");
        server_status = 0;
    }
}

void MainWindow::newuser()
{
    if(server_status==1){
        qDebug() << QString::fromUtf8("У нас новое соединение!");
        ui->textinfo->append(QString::fromUtf8("У нас новое соединение!"));
        QTcpSocket* clientSocket=tcpServer->nextPendingConnection();
        int idusersocs=clientSocket->socketDescriptor();
        SClients[idusersocs]=clientSocket;

        connect(SClients[idusersocs],SIGNAL(readyRead()),this, SLOT(slotReadClient()));
    }
}

void MainWindow::slotReadClient()
{
    QTcpSocket* clientSocket = (QTcpSocket*)sender();
    //int idusersocs=clientSocket->socketDescriptor();
    QTextStream os(clientSocket);
    os.setAutoDetectUnicode(true);
    os << QDateTime::currentDateTime().toString() << "\n";

    QString str = clientSocket->readAll();

    ui->textinfo->append("ReadClient:" + str + "\n\r");
    // Если нужно закрыть сокет
    //clientSocket->close();
    //SClients.remove(idusersocs);

    QString rez = parserCommand(str);

    os << rez << "\n";
}

QString MainWindow::parserCommand(QString message)
{
    QString command;
    QString filename;
    QString value;
    QString rez = "";
    QString homeDir = QDir::homePath();

    QStringList strList = message.split( " " );
    command = strList.value( strList.length() - 3 );
    filename = homeDir + "/" + strList.value( strList.length() - 2 );
    value = strList.value( strList.length() - 1 );

    if(command == "get"){
        rez = printFile(filename);
    }
    else if(command == "rewrite"){
        rewriteFile(filename, value);
        rez = printFile(filename);
    }
    else if(command == "push"){
        pushFile(filename, value);
        rez = printFile(filename);
    }
    else {
        ui->textinfo->append("invalid command\n\r");
    }

    return rez;
}

QString MainWindow::printFile(QString filename)
{
    QString rez;
    QFile file(filename);

    if(!file.open(QIODevice::ReadOnly))
    {
        ui->textinfo->append("Ошибка открытия для чтения\n\r");
        qDebug() << "Ошибка открытия для чтения";
    }

    QByteArray a = file.readAll();
    rez = QString::fromUtf8(a.data());

    file.close();

    return rez;
}

void MainWindow::rewriteFile(QString filename, QString str)
{
    QFile file(filename);

    if (file.open(QIODevice::WriteOnly))
    {
        QByteArray a = str.toUtf8();
        file.write(a);
    }
    else
    {
        ui->textinfo->append("Ошибка открытия для записи\n\r");
        qDebug() << "Ошибка открытия для записи";
    }

    file.close();
}

void MainWindow::pushFile(QString filename, QString str)
{
    QFile file(filename);

    if (file.open(QIODevice::Append))
    {
        QByteArray a = str.toUtf8();
        file.write(a);
    }
    else
    {
        ui->textinfo->append("Ошибка открытия для записи\n\r");
        qDebug() << "Ошибка открытия для записи";
    }

    file.close();
}

