#include <progbase-cpp/net.h>
#include <iostream>
#include "tcp_client.h"

using namespace std;
using namespace progbase::net;

int tcp_client(void) {
    TcpClient client;
    NetMessage message(1000);
    try {
        client.connect(IpAddress("127.0.0.1", 1337));
		string clientMessage;
        getline(cin, clientMessage);
        message.setDataString(clientMessage);
        client.send(message);
        cout << ">> Request sent" << endl;
        do {
            client.receive(message);
            std::string str = message.dataAsString();
            cout << ">> Received " << str.length() << " bytes: " << endl << str << endl;
        } while (!message.isEmpty());
    } catch(NetException const & exc) {
        cerr << exc.what() << endl;
    }
    return 0;
}