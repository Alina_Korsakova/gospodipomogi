#include <progbase-cpp/net.h>
#include <iostream>
#include <ctime>
#include <fstream>
#include <climits>
#include <cmath>
#include "tcp_server.h"
#include "convert.h"
#include "convert.h"
#include <jansson.h>

using namespace progbase::net;
using namespace std;


void tcp_server(void) {
    const int serverPort = 1337;
    TcpListener tcpListener;
    NetMessage message(2048);
    try {
        tcpListener.bind(IpAddress("127.0.0.1", serverPort));
        tcpListener.start();
        while (true) {
            cout << "Listening at " << serverPort << "..." << endl;
            TcpClient * tcpClient = tcpListener.accept();
            tcpClient->receive(message);
            cout << "Received: " << endl << message.dataAsString() << endl;
            string data = message.dataAsString();
            // cout << "data : " << data;
            // string response = createResponse(data);
            string response = parseMessage(data);
            message.setDataString(response);
            //printStack(&stack);
            tcpClient->send(message);
            delete tcpClient;
        }
    } catch(NetException const & exception) {
        cerr << exception.what() << endl;
    }
}

// string readFile(const char * filePath) {
//     string line;
//     string toReturn;
//     ifstream myfile (filePath);
//     if (!myfile.is_open()){
//         return "Can't open file";
//     }
//     while (! myfile.eof() ){
//         getline (myfile,line);
//         toReturn += line;
//     }
//     myfile.close();
//     return toReturn;
// }