#include "convert.h"
#include <progbase-cpp/net.h>
#include <vector>
#include <cctype>
#include <fstream> // http://www.cplusplus.com/reference/iostream/fstream/
#include <cstdlib> // http://www.cplusplus.com/reference/clibrary/cstdlib/

using namespace std;
using namespace progbase::net;

// bool is_number(string str)
// {
// 	for(int i = 0;i < (strlen(str) - 1);i++) {
// 		if((int)str[i] < 10) {
// 			return true;
// 		} else {
// 			return false;
// 		}
// 	}
// }
// typedef struct {
//     string ;
//     string ;
// } request;

string parseMessage(string message) {
	string command;
	string value;
	int delimeter = message.find(" ");
	// cout << "message : " << message << endl;
	// cout << "message.length() : " << message.length() << endl;
	// cout << "delimeter : " << delimeter;
	// cout << "message.substr(0, delimeter); : " << message.substr(0, delimeter) << endl;
	// cout << "message.substr(delimeter, message.length()); : " << message.substr(delimeter + 1, message.length()) << endl;
		
		command = message.substr(0, delimeter);
		value = message.substr(delimeter + 1, message.length());
		int delimeterr = value.find(" ");
		string filename = value.substr(0, delimeterr);
		string str = value.substr(delimeterr + 1, message.length());

		if(command == "get"){
			printFile(filename);
		}
		else if(command == "rewrite"){
			rewriteFile(filename, str);
			printFile(filename);
		}
		else if(command == "push"){
			pushFile(filename, str);
			printFile(filename);
		}
		else {
			return "invalid command";
		}
	 return "kek";
}


void printFile(string filename){
	string buff;
    ifstream file(filename); 
	if (!file.is_open()) 
        cout << "File does not exist\n";
	else{
		while(getline(file, buff)){
			cout << buff << endl;
		}
		file.close();
	}
}

void rewriteFile(string filename, string str){
    ofstream file(filename, ios_base::out | ios_base::trunc); 
	if (!file.is_open()) 
        cout << "File does not exist\n";
	else{
		file << str;
		file.close();
	}
}

void pushFile(string filename, string str){
	ofstream file(filename, ios_base::app);
	if (!file.is_open()) 
        cout << "File does not exist\n";
	else{
		// while(getline(file, buff)){
		// 	cout << buff << endl;
		// }
		file << "\n" << str;
		file.close();
	}
}