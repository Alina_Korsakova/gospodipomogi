#ifndef TCP_SERVER_H
#define TCP_SERVER_H

#include <iostream>
#include <vector>

using namespace std;

void tcp_server(void);
string serverInformation(void);
string fileInformation(void);
string fileNumbers(void);
string readFile(const char * filePath);

#endif // TCP_SERVER_H