#ifndef REQUEST_H
#define REQUEST_H

#include <iostream>
#include <vector>

using namespace std;

// string createResponse(string message);
string parseMessage(string message);
void printFile(string filename);
void rewriteFile(string filename, string str);
void pushFile(string filename, string str);

#endif // REQUEST_H