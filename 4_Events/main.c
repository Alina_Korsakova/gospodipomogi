
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <events.h>
#include <math.h>
#include "list.h"
#include <progbase/console.h>

/* custom constant event type ids*/
enum {
    KeyInputEventTypeId,
	RandomEventTypeId,
	CustomEventTypeId
};

typedef struct Timer Timer;
struct Timer {
	int id;
	int timeCounter;
};

typedef struct{
	int x;
	int y;
}  Coordinate;

typedef struct{
	int x;
	int y;
    int r;
}  Circle;

List * coordinates;
List * circle;

/* event handler functions prototypes */
void KeyInputHandler_update(EventHandler * self, Event * event);
void UpdatePrintHandler_update(EventHandler * self, Event * event);
void Timer_handleEvent(EventHandler * self, Event * event);

/*основные обработчики*/
void RandomGenerator_update(EventHandler * self, Event * event);        //обработчик 1
void RandomCircleGenerator_update(EventHandler * self, Event * event);  //обработчик 2
void CustomHandler_handleEvent(EventHandler * self, Event * event);     //обработчик 3

void Calc (List * c, List * circle);

int main(void) {

    // startup event system
    EventSystem_init();

    // add event handlers
	EventSystem_addHandler(EventHandler_new(NULL, NULL, UpdatePrintHandler_update));   
	EventSystem_addHandler(EventHandler_new(NULL, NULL, KeyInputHandler_update));    
    
    coordinates = List_new();
	EventSystem_addHandler(EventHandler_new(coordinates, free, RandomGenerator_update));

    circle = List_new();
    EventSystem_addHandler(EventHandler_new(circle, free, RandomCircleGenerator_update));

	Timer * timer = malloc(sizeof(Timer));
	timer->id = 0;
	timer->timeCounter = 10;

	EventSystem_addHandler(EventHandler_new(timer, free, Timer_handleEvent));
	EventSystem_addHandler(EventHandler_new(NULL, NULL, CustomHandler_handleEvent));

    // start infinite event loop
    EventSystem_loop();

    // cleanup event system
    EventSystem_cleanup();
    return 0;
}

/* event handlers functions implementations */

void UpdatePrintHandler_update(EventHandler * self, Event * event) {
	switch (event->type) {
		case StartEventTypeId: {
			puts("");
			puts("<<<START!>>>");
			puts("");
			break;
		}
		case ExitEventTypeId: {
			puts("");
			puts("<<<EXIT!>>>");
			puts("");
			break;
		}
	}
}

void KeyInputHandler_update(EventHandler * self, Event * event) {
	if (conIsKeyDown()) {  // non-blocking key input check
		char * keyCode = malloc(sizeof(char));
		*keyCode = getchar();
		EventSystem_emit(Event_new(self, KeyInputEventTypeId, keyCode, free));
	}
}

void RandomGenerator_update(EventHandler * self, Event * event) {
	switch (event->type) {
		case StartEventTypeId: {
			srand(time(0));
			break;
		}
		case UpdateEventTypeId: {
				
            Coordinate * c = malloc(sizeof(Coordinate));
            c->x = rand() % 200 - 100;
            c->y = rand() % 200 - 100;

            //printf("%i ", c->x);
            //printf("%i \n", c->y);

            List_add(self->data, c);

			break;
		}
	}
}

void RandomCircleGenerator_update(EventHandler * self, Event * event) {
	switch (event->type) {
		case StartEventTypeId: {
			srand(time(0));
			break;
		}
        case UpdateEventTypeId: {

            Circle * c = malloc(sizeof(Circle));
            c->x = rand() % 200 - 100;
            c->y = rand() % 200 - 100;
            c->r = abs(rand() % 100 - 100);

            //printf("%i ", c->x);
            //printf("%i ", c->y);
            //printf("%i \n", c->r);

            if (!List_contains(self->data, c))
            {
                List_add(self->data, c);
                EventSystem_emit(Event_new(self, RandomEventTypeId, NULL, NULL));
            }

			break;
		}

	}
}

void CustomHandler_handleEvent(EventHandler * self, Event * event) {
	switch (event->type) {
		case KeyInputEventTypeId: {
			char * keyCodePtr = (char *)event->data;
			char keyCode = *keyCodePtr;

			if (keyCode == 'q') {
				EventSystem_exit();
			}
			printf("Key pressed `%c` [%i]\n", keyCode, keyCode);
			break;
		}
		case RandomEventTypeId: {
			Calc (coordinates, circle);
			break;
		}        
	} 
}

void Timer_handleEvent(EventHandler * self, Event * event) {
	switch(event->type) {
		case UpdateEventTypeId: {
			Timer * timer = (Timer *)self->data;
			timer->timeCounter -= 1;
			if (timer->timeCounter == 0) {
				printf("\nTimer #%i: COMPLETED!\n", timer->id); 
                Timer * timer = malloc(sizeof(Timer));
				timer->id = 0;
				timer->timeCounter = 10;
				EventSystem_addHandler(EventHandler_new(timer, free, Timer_handleEvent));
			}
			break;
		}
	}
}

void Calc (List * c, List * circle)
{
    int d = 0;
    int count = 0;

    Circle *cc = List_get(circle, List_count(circle) - 1);

    //printf("%i ", cc->x);
    //printf("%i ", cc->y);
    //printf("%i \n", cc->r);

    puts("");

    // for(int i = 0; i < List_count(c); i++)
    // {
    //     Coordinate *t = List_get(c, i);
    //     printf("%i ", t->x);
    //     printf("%i \n", t->y);
    // }

    printf("Count all: #%i \n", List_count(c));
    puts("");

    
    for(int i = 0; i < List_count(c); i++)
    {
        d = 0;
        Coordinate *t = List_get(c, i);

        d = sqrt((pow((t->x - cc->x), 2) + pow((t->y - cc->y), 2))/*, 1/2*/);
        if (d < cc->r)
        {
            count++;
        }
    }

    printf("Count : #%i \n", count);

    puts("");
}

