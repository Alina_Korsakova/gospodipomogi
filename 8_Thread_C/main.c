#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <ctype.h>

sem_t sem;

void * thread_func(void *arg)
{ 
    //sem_wait(&sem);
    int i;
    int loc_id = * (int *) arg;
    char ch = 'a';

    //sem_post(&sem);
    for (i = 0; i < loc_id; i++) {
        if (loc_id < 5)
            printf("%c", ch);
        else
            printf("%c", (char)toupper(ch));
    }
    printf("\n");
    sem_post(&sem);
}

int main(int argc, char * argv[])
{ 
    char c;

    int id, result;
    pthread_t thread1, thread2;

    sem_init(&sem, 0, 0);

    while(c != 'q')
    //while(1)
    //while(c != EOF)
    {
        id = 5;
        result = pthread_create(&thread1, NULL, thread_func, &id);
        if (result != 0) {
            perror("Creating the first thread");
            return EXIT_FAILURE;
        }

        sem_wait(&sem);

        id = 2;
        result = pthread_create(&thread2, NULL, thread_func, &id);
        if (result != 0) {
            perror("Creating the second thread");
            return EXIT_FAILURE;
        }

        sem_wait(&sem);

        result = pthread_join(thread1, NULL);
        if (result != 0) {
            perror("Joining the first thread");
            return EXIT_FAILURE;
        }

        result = pthread_join(thread2, NULL);
        if (result != 0) {
            perror("Joining the second thread");
            return EXIT_FAILURE;
        }

        c = getchar();
    }

    sem_destroy(&sem);
    
    printf("Done\n");
    return EXIT_SUCCESS;
}