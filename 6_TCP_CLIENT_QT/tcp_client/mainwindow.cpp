#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setCursorPos();

    myClient("127.0.0.1", 33333);

    connect(ui->pbClear, SIGNAL(clicked(bool)), this, SLOT(clearForm()));
    connect(ui->pbSend, SIGNAL(clicked(bool)), this, SLOT(sendForm()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::clearForm()
{
    QString str = "";
    ui->leCommand->setText(str);
    ui->leFileName->setText(str);
    ui->leParam->setText(str);
    ui->lwRez->clear();
}

void MainWindow::setCursorPos()
{
    ui->leCommand->setCursorPosition(0);
    ui->leFileName->setCursorPosition(0);
    ui->leParam->setCursorPosition(0);
}

void MainWindow::controlAddItem(QString str)
{
    QListWidgetItem *item = new QListWidgetItem(str);
    ui->lwRez->addItem(item);
}

void MainWindow::myClient(const QString& strHost, int nPort)
{
    m_pTcpSocket = new QTcpSocket(this);

    m_pTcpSocket->connectToHost(strHost, nPort);

    connect(m_pTcpSocket, SIGNAL(connected()), SLOT(slotConnected()));
    connect(m_pTcpSocket, SIGNAL(readyRead()), SLOT(slotReadyRead()));
    connect(m_pTcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(slotError(QAbstractSocket::SocketError)));
}

void MainWindow::slotConnected()
{
    controlAddItem("Received the connected() signal");
}

void MainWindow::slotError(QAbstractSocket::SocketError err)
{
    QString strError =
        "Error: " + (err == QAbstractSocket::HostNotFoundError ?
                     "The host was not found." :
                     err == QAbstractSocket::RemoteHostClosedError ?
                     "The remote host is closed." :
                     err == QAbstractSocket::ConnectionRefusedError ?
                     "The connection was refused." :
                     QString(m_pTcpSocket->errorString())
                    );
    controlAddItem(strError);
}

void MainWindow::slotReadyRead()
{
    QTextStream os(m_pTcpSocket);
    os.setAutoDetectUnicode(true);
    QString str =  m_pTcpSocket->readAll();
    controlAddItem(str);
}

void MainWindow::sendForm()
{
    //myClient("127.0.0.1", 33333);

    QString clientMessage = ui->leCommand->text() + " " + ui->leFileName->text() + " " + ui->leParam->text();

    QTextStream os(m_pTcpSocket);
    os.setAutoDetectUnicode(true);
    //os << QDateTime::currentDateTime().toString() << "\n" << clientMessage << "\n";
    os << clientMessage << "\n";
}
