#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QTime>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QTcpSocket* m_pTcpSocket;

private slots:
    void clearForm();
    void sendForm();
    void setCursorPos();
    void controlAddItem(QString str);
    void myClient(const QString& strHost, int nPort);
    void slotConnected();
    void slotError(QAbstractSocket::SocketError);
    void slotReadyRead();
};

#endif // MAINWINDOW_H
