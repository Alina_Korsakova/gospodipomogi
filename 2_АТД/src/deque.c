#include "deque.h"


void addElementToEnd(char * str, char** arrayDeque, int* size){
	int k = 0;

	if(size == 0)
	  k = 1;

	if(*size >= SIZE_ARRAY)
		return;

	strcpy(arrayDeque[*size - k], str);

	*size = *size + 1;
}

void addElementToStart(char * str, char** arrayDeque, int* size){
	char* tmp;

	if(*size >= SIZE_ARRAY)
		return;

	char* temp = arrayDeque[0];

	strcpy(arrayDeque[0], str);

	*size = *size + 1;

	for(int i = 1; i < *size; i++){
		tmp = arrayDeque[i];
   		arrayDeque[i] = temp;
   		temp = tmp;
	}
}

void addElement(char * str, char** arrayDeque, int* size){

	if(strlen(str) % 2 == 0){
		addElementToEnd(str, arrayDeque, size);
	}	
	else{
		addElementToStart(str, arrayDeque, size);
	}
}