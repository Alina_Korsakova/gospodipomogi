#ifndef DEQUE_H
#define DEQUE_H

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>

#define SIZE_ARRAY 10
#define SIZE_STR 100

char** arrayDeque;

void addElementToEnd(char * str, char** arrayDeque, int* size);
void addElementToStart(char * str,  char** arrayDeque, int* size);
void addElement(char * str, char** arrayDeque, int* size);

#endif