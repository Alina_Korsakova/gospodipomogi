#include "student.h"
#include "deque.h"

int main(int argc, char** argv) {

	char buffer[50];
	char *array[] = {"aaaaaa aa", "bbbbbb","cccc"};
	int sizeArr = sizeof(array)/sizeof(char*);
	int size = 0;
	
	arrayDeque = (char**)malloc(SIZE_ARRAY * sizeof(char*));

	for(int i = 0; i < SIZE_ARRAY; i++){
		arrayDeque[i] = (char*)malloc(SIZE_STR * sizeof(char));
	}

    Student student = {
	 	.firstName = "Iisus",
		.lastName = "Christos",
	 	.age = 47,
	 	.score = 14.88
	 };

	 Student_toString(&student, buffer);
	 puts(buffer);
	 Student_print(Student_newFromString(buffer));


	for(int i = 0; i < sizeArr; i++){
		addElement(array[i], arrayDeque, &size);
	}

	for(int i = 0; i < SIZE_ARRAY; i++){
		printf("%s ", arrayDeque[i]);
	}

	free(arrayDeque);
    return (EXIT_SUCCESS);
}
